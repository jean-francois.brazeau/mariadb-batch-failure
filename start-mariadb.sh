#!/bin/bash
docker run -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=mydatabase -e MYSQL_USER=dbuser -e MYSQL_PASSWORD=secret -p 3306:3306 -d mariadb:10.5.6 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
